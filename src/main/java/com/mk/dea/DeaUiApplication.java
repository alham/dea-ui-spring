package com.mk.dea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class DeaUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeaUiApplication.class, args);
	}

}
